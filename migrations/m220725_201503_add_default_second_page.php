<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m220725_201503_add_default_second_page
 */
class m220725_201503_add_default_second_page extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->insert('usm_menu_items', [
            'id' => 100,
            'position' => 2,
            'title' => 'About',
            'alias' => 'about',
            'type' => 'plugin',
            'url' => '/site/about',
            'visible' => 1,
            'access' => 0,
            'created_at' => new Expression('UTC_TIMESTAMP()'),
            'updated_at' => new Expression('UTC_TIMESTAMP()'),
        ]);

        $this->insert('usm_redirects', [
            'active' => 1,
            'url' => 'about',
            'type' => 'menu_item',
            'menu_item_id' => 100,
            'forward' => 0,
            'generated' => 1,
            'created_at' => new Expression('UTC_TIMESTAMP()'),
            'updated_at' => new Expression('UTC_TIMESTAMP()'),
        ]);

        $this->insert('usm_menu_items', [
            'id' => 101,
            'position' => 3,
            'title' => 'Contact',
            'alias' => 'contact',
            'type' => 'plugin',
            'url' => '/site/contact',
            'visible' => 1,
            'access' => 0,
            'created_at' => new Expression('UTC_TIMESTAMP()'),
            'updated_at' => new Expression('UTC_TIMESTAMP()'),
        ]);

        $this->insert('usm_redirects', [
            'active' => 1,
            'url' => 'contact',
            'type' => 'menu_item',
            'menu_item_id' => 101,
            'forward' => 0,
            'generated' => 1,
            'created_at' => new Expression('UTC_TIMESTAMP()'),
            'updated_at' => new Expression('UTC_TIMESTAMP()'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->delete('usm_redirects', [
            'menu_item_id' => 100,
        ]);

        $this->delete('usm_menu_items', [
            'id' => 100,
        ]);

        $this->delete('usm_redirects', [
            'menu_item_id' => 101,
        ]);

        $this->delete('usm_menu_items', [
            'id' => 101,
        ]);
    }
}
