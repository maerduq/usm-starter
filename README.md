# USM Starter project by dePaul Programming

Get started quickly with an USM enabled Yii2 project.

Published under [MIT license](LICENSE).

[📣 Release notes](RELEASE-NOTES.md)

## Features

- Ultimate Site Management pre-configured.
- DotEnv for management of environment variables.
- Docker compose with
  - PHP container running your Yii2 application
  - MySQL container with database stored in local `./runtime/database` folder
  - Mailpit, a mail debugging tool

## Prerequisites

- [Composer](http://getcomposer.org/), you may install it by following the instructions
  at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).
- [Docker](https://www.docker.com/)

## Installation

You can install this project template using the following command:

```
$ composer create-project --prefer-dist maerduq/usm-starter my-app-name
```

## Getting started for local development

> `$` indicates a command to run on your host computer, `#` indicates a command to run in your application's docker container

- `$ cp .env-dev.example .env` - to setup you environment variables file.
- `$ docker compose up -d` - to start your docker containers
- Open a bash shell in your `php` docker container by executing `$ docker exec -it <containerId> bash`
- `# bash script-dev-setup` - for quick setup (run in your php container)
- Now visit your website on [http://localhost:8000/](http://localhost:8000/)
- A very useful mail debugger called **mailpit** is available on [http://localhost:8025/](http://localhost:8025/)
- And your mysql server is accessible via [localhost:3006](localhost:3006)

## Getting started for production

- `$ cp .env-prod.example .env` - to setup you environment variables file.
- Edit the values in `.env` to your preference.
- `$ bash deploy` - to install everything.

## Other available bash scripts

- `# bash script-dev-watch` - for watchers used during development
- `# php yii message messages/config.php` - generate translation files from your views. [Learn more](https://www.yiiframework.com/doc/guide/2.0/en/tutorial-i18n).

## Application configuration

Use the following command to create your application configuration file:

```
$ cp .env.example .env
```

Change the environment variables as you like using the explanation of the variables below.

### Variables

| Name            | Description                                             |
| --------------- | ------------------------------------------------------- |
| YII_DEBUG       | Whether Yii should run in debug mode (1 or 0)           |
| YII_ENV         | The Yii environment setting to run (dev, test, or prod) |
| DB_HOST         | Host name of your database (e.g. localhost)             |
| DB_NAME         | Name of your database                                   |
| DB_USERNAME     | Username to access your database                        |
| DB_PASSWORD     | Password to access your database                        |
| DB_CHARSET      | Charset of your database (e.g. utf8mb4)                 |
| DB_SCHEMA_CACHE | Whether the schema of your database should be cached    |
| ADMIN_EMAIL     | The administrator's email                               |

## Directory Structure

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      models/             contains model classes
      runtime/            contains files generated during runtime
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources

## Contributors

- Paul Marcelis

## Testing

### Setup

- Create database `app_test`
- `# php tests/bin/yii migrate-usm --interactive=0` - to setup structure for test database
- `# php tests/bin/yii migrate --interactive=0` - to setup structure for test database
- `# composer test` - to run all tests
- `# composer test -- unit` - to only run unit tests

### Additional commands

- `# php vendor/bin/codecept run` - to run all tests
- `# php vendor/bin/codecept run --steps` - to run all tests with showing every individual step
- `# php vendor/bin/codecept run unit` - to run only unit tests
- `# php vendor/bin/codecept run functional` - to run only functional tests
- `# php vendor/bin/codecept run unit widgets/AlertTest` - to run a specific test file
- `# php vendor/bin/codecept run unit widgets/AlertTest:testSingleErrorMessage` - to run a single test
- `# php vendor/bin/codecept run --debug` - to run with debug info
- `# php tests/bin/yii fixture Customer` - to load CustomerFixture data
- `# php tests/bin/yii fixture/unload "*"` - to unload all data

### Doc pointers

- [About Yii2 Codeception testing](https://codeception.com/for/yii)
- [About the Yii2 module for Codeception](https://codeception.com/docs/modules/Yii2)
- [About fixtures, that prescribe data you use to do tests](https://www.yiiframework.com/doc/guide/2.0/en/test-fixtures)
- [About debugging](https://codeception.com/docs/Debugging)
