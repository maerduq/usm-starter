<?php

/** @var yii\web\View $this */

use maerduq\usm\widgets\Textblock;

$this->title = \Yii::t('app', 'About');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php Textblock::begin(['name' => 'about']) ?>
<p>This is the about page.</p>
<p><i>This is a textblock, which you can edit using the editor.</i></p>
<?php Textblock::end() ?>