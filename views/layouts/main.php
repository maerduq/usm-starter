<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use maerduq\usm\components\Usm;
use maerduq\usm\UsmModule;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;
use yii\helpers\Url;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <title><?= Html::encode($this->title . " | " . Yii::$app->name) ?></title>
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <div id="title">
        <div class="container">
            <h1><?= Html::a(\Yii::$app->name, '@web/') ?></h1>
        </div>
    </div>

    <header id="header">
        <?php
        NavBar::begin([
            'brandLabel' => false,
            'options' => ['class' => 'navbar-expand-lg bg-body-tertiary'],
            'collapseOptions' => ['class' => 'justify-content-between'],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => Usm::getMenu(),
        ]);

        $itemsRight = [];
        if (count(UsmModule::getInstance()->languages) > 1) {
            $langChooserItems = [];
            foreach (UsmModule::getInstance()->languages as $lang) {
                if ($lang == Yii::$app->language) {
                    continue;
                }

                $langChooserItems[] = [
                    'label' => $lang,
                    'url' => Url::current(['lang' => $lang])
                ];
            }
            $itemsRight = array_merge([[
                'label' => \Yii::t('app', 'Language: {0}', [Yii::$app->language]),
                'items' => $langChooserItems,
            ]], $itemsRight);
        }

        if (Usm::isUserAdmin()) {
            $itemsRight[] = ['label' => \Yii::t('app', 'Admin'), 'url' => ['/usm']];
        }

        $itemsRight[] = (Yii::$app->user->isGuest) ? ['label' => \Yii::t('app', 'Login'), 'url' => ['/site/login']] : ('<li class="nav-item">'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                \Yii::t('app', 'Log out'),
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        );

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => $itemsRight
        ]);
        NavBar::end();
        ?>
    </header>

    <main id="main" class="flex-shrink-0" role="main">
        <div class="container">
            <?php
            if (isset($this->params['pageHeader'])) {
                $header = $this->params['pageHeader'];
            } else {
                $header = $this->title;
            }
            ?>
            <?php if ($header != null) : ?>
                <div class="page-header">
                    <h1><?= $header ?></h1>
                </div>
            <?php endif ?>
            <?php if (!empty($this->params['breadcrumbs'])) : ?>
                <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
            <?php endif ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>

    <footer id="footer" class="mt-auto py-4 bg-light">
        <div class="container">
            <div class="text-muted text-end">
                Made by <?= Html::a('dePaul Programming', 'https://www.depaul.nl', ['target' => '_blank']) ?>.
            </div>
        </div>
    </footer>

    <?= Usm::renderToolbar($this) ?>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>