# Release notes

## v3.0.0 @ 18 September 2023

### Added

- Added Mailpit to docker-compose setup for easy mail debugging.
- Make a separate dev (`.env-dev.example`) and prod (`.env-prod.example`) example file for the `.env` file.
- Added more environment variables for mailing.
- Added Yii's example _contact_ and _about_ pages.
- Added functional tests
- Added bootstrapping the yii2-bootstrap5 translations
- Added some more stuff from the [yii2-basic-app](https://github.com/yiisoft/yii2-app-basic) boilerplate.

### Changed

- Upgrade support from PHP 7.4 to PHP 8.2
- Implemented php docker image setup steps in a Dockerfile
- Restructured development and deployment scripts
- Replaced deprecated `yiisoft/yii2-swiftmailer` with `yiisoft/yii2-symfonymailer` package.
- Replaced `yiisoft/yii2-bootstrap` with `yiisoft/yii2-bootstrap5` package, which was about time.
- Updated `README.md` with explicit production deployment steps
- Changed default pages added to the USM menu

### Fixed

- Fixed the messages config file
