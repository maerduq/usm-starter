<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'usm-starter',
    'name' => 'USM Starter',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'usm', \yii\bootstrap5\i18n\TranslationBootstrap::class],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@usm' => '@vendor/maerduq/usm/src',
    ],
    'language' => 'nl',
    'modules' => [
        'usm' => [
            'class' => \maerduq\usm\UsmModule::class,
            'languages' => ['nl', 'en'],
            'layout_container' => '/main',
            'availablePagesForMenu' => [
                ['name' => 'About page', 'actionId' => '/site/about'],
                ['name' => 'Contact page', 'actionId' => '/site/contact'],
            ],
            /* Option 1: Yii style access control */
            'access_type' => 'yii',
            'access_admin_check' => function () {
                if (Yii::$app->user->identity == null) {
                    return false;
                }
                return (Yii::$app->user->identity->username == 'admin');
            }
            /* Option 2: USM access control */
            // 'access_type' => 'usm2',
            // 'access_password' => '', //run command `php yii usm/pwtool/hash <password>` to determine this value
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'formatter' => [
            'class' => \maerduq\usm\components\Formatter::class,
        ],
        'cache' => [
            'class' => \yii\caching\FileCache::class,
        ],
        'user' => [
            /* Option 1: Yii style access control */
            'identityClass' => \app\models\User::class,
            /* Option 2: USM access control */
            // 'identityClass' => \maerduq\usm\models\User::class,
            // 'loginUrl' => ['/usm/global/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => \yii\symfonymailer\Mailer::class,
            'transport' => [
                'dsn' => ($_ENV['MAIL_ENCRYPTED'] ? 'smtps' : 'smtp') . '://' . $_ENV['MAIL_USERNAME'] . ':' . $_ENV['MAIL_PASSWORD'] . '@' . $_ENV['MAIL_HOST'] . ':' . $_ENV['MAIL_PORT'],
            ],
            'viewPath' => '@app/mail',
            'useFileTransport' => $_ENV['MAIL_TO_FILE'] ? true : false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => \yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => \maerduq\usm\components\RedirectRule::class],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '172.*.0.1', '192.168.*.1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '172.*.0.1', '192.168.*.1'],
    ];
}

return $config;
