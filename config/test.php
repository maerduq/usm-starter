<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/test_db.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'usm-starter-test',
    'name' => 'USM Starter Test',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['usm'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@usm' => '@vendor/maerduq/usm/src'
    ],
    'language' => 'en-US',
    'controllerMap' => [
        'migrate-usm' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => ['@usm/migrations'],
            'migrationTable' => 'migration_usm',
        ]
    ],
    'modules' => [
        'usm' => [
            'class' => 'maerduq\usm\UsmModule',
            'layout_container' => '/main',
            'access_type' => 'yii',
            'access_admin_check' => function () {
                if (Yii::$app->user->identity == null) {
                    return false;
                }
                return (Yii::$app->user->identity->username == 'admin');
            }
        ],
    ],
    'components' => [
        'db' => $db,
        'mailer' => [
            'class' => \yii\symfonymailer\Mailer::class,
            'viewPath' => '@app/mail',
            // send all mails to a file by default.
            'useFileTransport' => true,
            'messageClass' => 'yii\symfonymailer\Message'
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
        'user' => [
            'identityClass' => 'app\models\User',
        ],
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
            // but if you absolutely need it set cookie domain to localhost
            /*
            'csrfCookie' => [
                'domain' => 'localhost',
            ],
            */
        ],
        'formatter' => [
            'class' => 'maerduq\usm\components\Formatter',
        ],
        'urlManager' => [
            'rules' => [
                ['class' => 'maerduq\usm\components\RedirectRule'],
            ],
        ],
    ],
    'params' => $params,
];
